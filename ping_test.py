import functools
from concurrent import futures
import subprocess
import pandas as pd
import numpy
import time

ont1 = pd.read_csv('/home/hbadmin/GoogleDrive/tracker.csv', dtype=object)
ont = ont1.replace(numpy.nan, '', regex=True)
ip_list = []

row = -1
for ip in ont['IP Address']:
    row += 1
    if 'Antminer' not in ont.at[row, 'machineType']:
        pass
    else:
        ip_list.append(ip)

print(len(ip_list))
for host in ip_list:
    print(host)

offline = 0

def ping(hostname):
    p = subprocess.Popen(["ping", "-n", "1", "-w", "1000",
                          hostname],
                         stdout=subprocess.DEVNULL,
                         stderr=subprocess.DEVNULL)
    return p.wait() == 0


with futures.ThreadPoolExecutor(max_workers=8) as executor:
    start = time.time()
    futs = [
        (host, executor.submit(functools.partial(ping, host)))
        for host in ip_list
    ]

    for ip, f in futs:
        if not f.result():
            offline += 1
    print('time elapsed ' + str(time.time() - start))

# print('just before futures tries to log in to each one')
# with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
#     time1 = time.time()
#     futures = {executor.submit(
#         self.set_config, ip): ip for ip in self.ipdict}
# print('futures just ended')